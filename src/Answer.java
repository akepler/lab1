import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

   public static void main (String[] param) throws InterruptedException {

      // TODO!!! Solutions to small problems 
      //   that do not need an independent method!
    
      // conversion double -> String

       double myDouble = 1.15;
       String str2dbl = String.valueOf(myDouble);
       System.out.println(String.format("Double2String: %s", str2dbl));

       // conversion String -> int
       int myInt = Integer.parseInt("1111");
       System.out.println(String.format("String2Int: %s ", myInt));
       // "hh:mm:ss"
       DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
       Date date = new Date();
       System.out.println(String.format("Time is %s", dateFormat.format(date)));
      // cos 45 deg
       System.out.println(String.format("Cos 45 deg: %s", Math.cos(Math.toRadians(45))));

      // table of square roots
       int [] table = new int[10];


       for (int i = 0; i <table.length ; i++) {
           table[i] = (int) Math.pow(i,2);
       }

       System.out.println(Arrays.toString(table));


      String firstString = "ABcd12";
      String result = reverseCase(firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
       Thread.sleep(500);

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (new Integer (generaator.nextInt(1000)));
      }

      // minimal element
       System.out.println(String.format("Min: %s", Collections.min(randList)));

      // HashMap tasks:
      //    create
       HashMap hm = new HashMap();
       hm.put("Jaan", new Integer(30));
       hm.put("Jüri", new Integer(20));
       hm.put("Helmut", new Integer(35));
      //    print all keys
       System.out.println(hm.keySet());
      //    remove a key
       hm.remove("Helmut");

      //    print all pairs
       // Random help from http://www.tutorialspoint.com/java/java_hashmap_class.htm
       // Get a set of the entries
       Set set = hm.entrySet();
       // Get an iterator
       Iterator i = set.iterator();
       // Display elements
       while(i.hasNext()) {
           Map.Entry me = (Map.Entry)i.next();
           System.out.print(me.getKey() + ": ");
           System.out.println(me.getValue());
       }




      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {


      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text)
   {
       int lenght = 0;
       //In most cases.
       String [] newlist = text.split("\\s+");
       lenght = newlist.length;
       //If empty entry.
       for (int i = 0; i < newlist.length; i++) {
           if (newlist[i].equals("")) {
               lenght--;
           }
       }



      return lenght;
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    * Random help. http://stackoverflow.com/questions/677561/is-there-an-existing-library-method-that-checks-if-a-string-is-all-upper-case-or
    */
   public static String reverseCase (String s) {
       char[] charArray = s.toCharArray();
       StringBuilder mystring = new StringBuilder();

       for (int i = 0; i < charArray.length ; i++) {
           if (Character.isUpperCase(charArray[i])) {
               //if lower the upper
               mystring.append(Character.toLowerCase(charArray[i]));
           } else if (Character.isLowerCase(charArray[i])) {
               //if upper then lowe
               mystring.append(Character.toUpperCase(charArray[i]));
           } else  {
               //If no char.
               mystring.append(charArray[i]);
           }

       }

       return mystring.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {

       Collections.reverse(list);

   }
}
